package com.example.order.entity;

import com.example.common.core.entity.BaseEntity;
import lombok.Data;

/**
 * @author 19464
 * @Classname OrderEntity
 * @Description TODO
 * @Date 2021/6/24 15:17
 */
@Data
public class OrderEntity extends BaseEntity{

    private String orderNo;
}
