package com.example.order.entity;

public enum OrderStatusEnum {

    WaitRenewalInsurance(-3 , "待续保"),
    PayLock(-2 , "支付锁定中"),
    DisCard(-1 , "已废弃"),
    WaitPay(0 , "待支付");

    /**
     * 状态数值
     */
    private int status;


    /**
     * 描述
     */
    private String description;


    private OrderStatusEnum(){

    }

    private OrderStatusEnum(int status, String description){
        this.status = status;
        this.description = description;
    }

}
