package com.example.order.dao.impl;

import com.example.common.core.base.AbstractDaoImpl;
import com.example.order.dao.OrderDao;
import com.example.order.entity.OrderEntity;
import org.springframework.stereotype.Repository;

/**
 * @author chenrute
 * @Classname OrderDaoImpl
 * @Description TODO
 * @Date 2021/6/26 16:31
 */
@Repository
public class OrderDaoImpl extends AbstractDaoImpl implements OrderDao {

    @Override
    public long insert(OrderEntity orderEntity) {
        return super.insert(orderEntity);
    }
}
