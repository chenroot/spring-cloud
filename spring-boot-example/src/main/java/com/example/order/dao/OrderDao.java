package com.example.order.dao;

import com.example.common.core.entity.BaseEntity;
import com.example.order.entity.OrderEntity;

public interface OrderDao {

    long insert(OrderEntity orderEntity);
}
