package com.example.insurance.hongkang.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chenrute
 * @Classname AccountInfo
 * @Description 账户信息
 * @Date 2021/6/26 15:22
 */
@Data
public class AccountInfo {

    /**
     * 账户编码 AC0011定期 AC0012活期 AC001 总账户
     */
    private String accountCode;

    /**
     * 账户属性 M主账户, F固定账户, D灵活账户
     */
    private String accountAttribute;

    /**
     * 账户价值（单位为分）
     */
    private BigDecimal accountValue;

    /**
     * 持续奖金(初始费用，单位为分)
     */
    private BigDecimal policyInitFee;
}
