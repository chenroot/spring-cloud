package com.example.insurance.hongkang.util;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.HexUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.AsymmetricAlgorithm;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class DigitalEnvelopeUtil {

    public static String PUBLIC_KEY;
    public static String PRIVATE_KEY;

    public final static String KEY = "key";
    public final static String DATA = "data";

    @Value("${hongkang.public.key:}")
    public void setPublicKey(String publicKey) {
        PUBLIC_KEY = publicKey;
    }

    @Value("${hongkang.private.key:}")
    public void setPrivateKey(String privateKey) {
        PRIVATE_KEY = privateKey;
    }

    public static Map<String, String> encrypt(String data) throws UnsupportedEncodingException {
        log.info("【数字信封加密】需要加密的数据:" + data);
        Map<String, String> resMap = new HashMap<>(3);
        SecretKey secretKey = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue());
        byte[] key = secretKey.getEncoded();
        RSA rsa = getRsaByStr();
        // 使用AES对明文进行加密
        String encryptStr = aesEncrypt(data, key);
        // 使用RSA对AES-KEY进行加密
        String encryptKey = rsaEncrypt(rsa, key);
        resMap.put(DATA, encryptStr);
        resMap.put(KEY, encryptKey);
        log.info("【数字信封加密】加密后的数据:" + JSON.toJSONString(resMap));
        return resMap;
    }

    public static String decrypt(String data, String key) {
        // 1进行解密
        RSA rsa = getRsaByStr();
        // 1.1使用RSA解密AES-KEY
        byte[] decryptKey = rsaDecrypt(rsa, key);
        // 1.2使用AES解密报文
        String decryptStr = aesDecrypt(data, decryptKey);
        return decryptStr;
    }

    private static String aesDecrypt(String data, byte[] decryptKey) {
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, decryptKey);
        //解密
        byte[] encrypt = Base64.decode(data);
        String decryptStr = aes.decryptStr(encrypt);
        log.info("AES解密后的明文：" + decryptStr);
        return decryptStr;
    }

    private static byte[] rsaDecrypt(RSA rsa, String encryptKey) {
        //私钥解密
        byte[] encrypt = Base64.decode(encryptKey);
        byte[] decrypt = rsa.decrypt(encrypt, KeyType.PrivateKey);
        log.info("RSA私钥解密：" + HexUtil.encodeHexStr(decrypt));
        return decrypt;
    }
    private static String rsaEncrypt(RSA rsa, byte[] key) throws UnsupportedEncodingException {
        //公钥加密
        byte[] encrypt = rsa.encrypt(key, KeyType.PublicKey);
        String encrStr = Base64.encode(encrypt);
        log.info("RSA公钥加密:" + encrStr);
        log.info("RSA公钥加密encode：" + URLEncoder.encode(encrStr, "UTF-8"));
        return encrStr;
    }

    private static String aesEncrypt(String data, byte[] key) throws UnsupportedEncodingException {
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        //加密
        byte[] encrypt = aes.encrypt(data);
        String encryptStr = Base64.encode(encrypt);
        log.info("AES加密后的密文：" + encryptStr);
        log.info("AES加密后的密文encode：" + URLEncoder.encode(encryptStr, "UTF-8"));
        return encryptStr;
    }

    private static RSA getRsaByStr() {
        return new RSA(AsymmetricAlgorithm.RSA_ECB_PKCS1.getValue(), PRIVATE_KEY, PUBLIC_KEY);
    }
}