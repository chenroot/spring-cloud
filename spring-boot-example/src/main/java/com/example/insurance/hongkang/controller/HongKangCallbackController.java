package com.example.insurance.hongkang.controller;

import com.alibaba.fastjson.JSON;
import com.example.common.config.DynamicConfig;
import com.example.common.core.base.BaseController;
import com.example.common.core.entity.JSONResult;
import com.example.common.util.RedisUtil;
import com.example.insurance.hongkang.entity.HongKangOrderDTO;
import com.example.insurance.hongkang.service.HongKangService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Classname HongKangCallbackController
 * @Description 弘康回调入口类
 * @Date 2021年6月26日
 * @author chenrute
 */
@Controller
@RequestMapping("/hongkang/callback")
@Slf4j
public class HongKangCallbackController extends BaseController {

    @Autowired
    private DynamicConfig dynamicConfig;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private HongKangService hongKangService;


    @RequestMapping("/createOrder")
    @ResponseBody
    public JSONResult createOrder(@RequestBody HongKangOrderDTO hongKangOrderDTO){
        //TODO
        log.info("请求参数:" + JSON.toJSONString(hongKangOrderDTO));
        try {
            hongKangService.createOrder(hongKangOrderDTO);
        }catch (Exception e){
            //TODO
            log.warn("出单回调失败:", e);
            return JSONResult.error("出单回调失败");
        }
        return JSONResult.success();
    }
}
