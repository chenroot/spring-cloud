package com.example.insurance.hongkang.entity;

import lombok.Data;

/**
 * @author chenrute
 * @Classname UserInfo
 * @Description 用户信息
 * @Date 2021/6/26 15:26
 */
@Data
public class UserInfo {

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别 （1:男；2:女）
     */
    private String sex;

    /**
     * 证件类型：
     * 0	身份证
     * 1	护照
     * 2	军人证（军官证）
     * 3	驾照
     * 4	户口本
     * 5	学生证
     * 6	工作证
     * 7	出生证
     * 8	其它
     * 9	无证件
     * A	士兵证
     * B	回乡证
     * C	临时身份证
     * D	警官证
     * E	港澳台通行证
     * F	外国人永久居留身份证
     * G	港澳台居民居住证
     */
    private String idType;

    /**
     * 证件号
     */
    private String idNo;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 出生日期 （yyyy-MM-dd）
     */
    private String birthday;

    /**
     * 联系地址
     */
    private String address;
}
