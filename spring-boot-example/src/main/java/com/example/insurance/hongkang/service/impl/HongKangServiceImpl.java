package com.example.insurance.hongkang.service.impl;

import com.example.insurance.hongkang.entity.HongKangOrderDTO;
import com.example.insurance.hongkang.service.HongKangService;
import com.example.order.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author chenrute
 * @Classname HongKangServiceImpl
 * @Description TODO
 * @Date 2021/6/26 16:24
 */
@Service
public class HongKangServiceImpl implements HongKangService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public long createOrder(HongKangOrderDTO hongKangOrderDTO) {
        return orderDao.insert(hongKangOrderDTO.transform());
    }
}
