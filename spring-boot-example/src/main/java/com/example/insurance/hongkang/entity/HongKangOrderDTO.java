package com.example.insurance.hongkang.entity;

import com.example.order.entity.OrderEntity;
import lombok.Data;

import java.util.List;

/**
 * @author chenrute
 * @Classname HongKangOrderDTO
 * @Description 弘康出单回调接收对象
 * @Date 2021/6/26 15:36
 */
@Data
public class HongKangOrderDTO{

    /**
     * 订单号（支付成功时必传）
     */
    private String orderNo;

    /**
     * 0支付成功 ，-1支付失败
     */
    private String payResult;

    /**
     * 保单号（支付成功时必传）
     */
    private String policyNo;

    /**
     * 	持续奖金(初始费用，单位为分)
     */
    private String policyInitFee;

    /**
     * 电子保单地址（支付成功时必传）
     */
    private String policyUrl;

    /**
     * yyyy-MM-dd 保单生效时间（支付成功时必传）
     */
    private String effectTime;

    /**
     * yyyy-MM-dd HH:mm:ss 支付成功时间（支付成功时必传）
     */
    private String payTime;

    /**
     * 渠道用户ID
     */
    private String channelUserId;

    /**
     * 推荐人编码
     */
    private String adviceCode;

    /**
     * 用户（投保人）信息【需要单独申请】
     */
    //private UserInfo userInfo;

    /**
     *	账户信息
     */
    private List<AccountInfo> accountInfo;

    public OrderEntity transform(){
        //TODO
        return new OrderEntity();
    }
}
