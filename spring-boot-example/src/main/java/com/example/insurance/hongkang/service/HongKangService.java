package com.example.insurance.hongkang.service;

import com.example.insurance.hongkang.entity.HongKangOrderDTO;

public interface HongKangService {

    long createOrder(HongKangOrderDTO hongKangOrderDTO);
}
