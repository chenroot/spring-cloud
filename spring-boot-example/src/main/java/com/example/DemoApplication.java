package com.example;

import com.example.common.core.utils.EnvironmentAwareUtil;
import com.example.common.core.utils.SpringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class DemoApplication {


	public static void main(String[] args) {
		EnvironmentAwareUtil.adjust();
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public SpringUtils springUtils(){
		return new SpringUtils();
	}
}
