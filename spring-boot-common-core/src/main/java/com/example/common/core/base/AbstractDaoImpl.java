package com.example.common.core.base;

import com.example.common.core.entity.BaseEntity;
import org.apache.ibatis.session.SqlSession;

import javax.annotation.Resource;

public abstract class AbstractDaoImpl<T extends BaseEntity> {

    @Resource(name = "materSqlSessionTemplate")
    private SqlSession readSessionTemplate;
    @Resource(name = "materSqlSessionTemplate")
    private SqlSession writeSessionTemplate;

    private static final String SQL_INSERT = "insert";

    public SqlSession getReadSessionTemplate() {
        return readSessionTemplate;
    }

    public void setReadSessionTemplate(SqlSession readSessionTemplate) {
        this.readSessionTemplate = readSessionTemplate;
    }

    public void setWriteSessionTemplate(SqlSession writeSessionTemplate) {
        this.writeSessionTemplate = writeSessionTemplate;
    }

    public SqlSession getWriteSessionTemplate() {
        return writeSessionTemplate;
    }

    /**
     * 新增对象
     * @param entity
     * @return
     */
    public long insert(T entity){
        int result = writeSessionTemplate.insert(getStatement(SQL_INSERT), entity);
        if(entity != null && entity.getId() != null && result > 0){
            return entity.getId();
        }
        return result;
    }



    /**
     * 获取Mapper命名空间
     * @param sqlId
     * @return
     */
    public String getStatement(String sqlId){
        return this.getClass().getName() + "." + sqlId;
    }
}
