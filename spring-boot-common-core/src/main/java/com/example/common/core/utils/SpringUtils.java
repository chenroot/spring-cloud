package com.example.common.core.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author 19464
 * @Classname SpringUtils
 * @Description TODO
 * @Date 2021/6/24 16:13
 */
public class SpringUtils implements ApplicationContextAware {
    private static ApplicationContext context;

    public static <T> T getBean(String beanName){
        return (T) context.getBean(beanName);
    }
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
