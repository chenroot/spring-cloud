package com.example.common.core.entity;

import lombok.Data;

/**
 * @author chenrute
 * @Classname JSONResult
 * @Description 返回对象
 * @Date 2021/6/26 15:47
 */
@Data
public class JSONResult {

    /**
     * 返回的code
     */
    private int code;

    /**
     * 描述信息
     */
    private String msg;

    /**
     * 返回的对象信息
     */
    private Object raws;

    public JSONResult(){

    }

    public JSONResult(int code){
        this.code = code;
    }

    public JSONResult(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public JSONResult(int code, String msg, Object raws){
        this.code = code;
        this.msg = msg;
        this.raws = raws;
    }

    public static JSONResult success(){
        // TODO
        return new JSONResult(0, "success");
    }

    public static JSONResult error(String msg){
        // TODO
        return new JSONResult(-1, msg);
    }
}
