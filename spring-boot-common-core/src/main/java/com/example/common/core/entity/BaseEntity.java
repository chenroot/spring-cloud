package com.example.common.core.entity;

import java.io.Serializable;

/**
 * 基础实体类
 */
public class BaseEntity implements Serializable {

    /**
     * 主键id
     */
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
